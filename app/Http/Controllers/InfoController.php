<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\OpenLeads;
use App\Models\Lead;
use App\Models\SphereAttr;
use App\Models\SphereAttrOptions;
use App\Models\SphereMask;
use Sentinel;

//use App\Http\Requests\Admin\ArticleRequest;

class InfoController extends Controller {
    
    public function index()
    {

        $user = Sentinel::getUser();

        if (empty($user)) {
            return redirect('auth/login');
        }
        
        $data = $this->getLeadData($user->id);

        return view('info.index')
            ->with('data',$data);
    }

    public function ajaxInfo($id){

        $user = Sentinel::getUser();

        $info  = $this->getLeadData($user->id);

        $view = view('info.info')
            ->with('data',$info);

        return response()->json(array(
            'success' => true,
            'html' => $view
        ));
    }

    function getLeadData($id){
        $lead = Lead::where('agent_id', $id)->first();
        if (OpenLeads::where('lead_id', $lead->id)->count() > 0) {
            $sphere_attributes = SphereAttr::where('sphere_id', $lead->sphere_id)->get();
            $data = array(
                'icon' => '',
                'date' => $lead->date,
                'name' => $lead->name,
                'phone' => $lead->phone(),
                'email' => $lead->email,
                'id' => $id,
            );

            $additional_fields = array();

            foreach ($sphere_attributes as $key => $attr) {

                $options = SphereAttrOptions::where('sphere_attr_id', $attr->id)
                    ->where('ctype', 'agent')
                    ->get();

                foreach ($options as $key => $option) {
                    if(SphereMask::where('fb_'.$attr->id.'_'.$option->id, 1)->where('user_id', $lead->id)->where('type', 'lead')->count() > 0){
                        $additional_fields[$attr->label][] = $option;
                    } 
                }
            }

            $data['additional_fields'] = $additional_fields;

        }else{
            $data = array();
        }
        return $data;
    }

}
