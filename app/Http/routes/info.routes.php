<?php
Route::get('info', ['as' => 'info.index', 'uses' => 'InfoController@index']);
Route::get('info/{id}', ['as' => 'info.ajaxInfo', 'uses' => 'InfoController@ajaxInfo']);
?>