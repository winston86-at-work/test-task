<!DOCTYPE html>
<html>
    <head>
        <title>Info table</title>
        <link href="https://fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">

        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }
            .info-clickable{
                cursor: pointer;
            }
            .info-hidden{
                display: none;
            }
        </style>
        <script type="text/javascript">
            $('.info-clickable').click(function(){
                var id = $(this).data('id');
                $.ajax({
                    url: id,
                    success: function(res){
                        viewInfo(res);
                    },
                    error: function(res){
                        console.log(res);
                    }
                });
            });
            function viewInfo(info){

                $('.info-hidden').modal(info.html);

            }
        </script>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <table class="table table-striped">
                    <thead class="thead-dark">
                        <tr>
                            <th>icon</th>
                            <th>date</th>
                            <th>name</th>
                            <th>phone</th>
                            <th>email</th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr data-id="{!! $data['id'] !!}" class="info-clickable">
                            <td></td>
                            <td>{!! $data['date'] !!}</td>
                            <td>{!! $data['name'] !!}</td>
                            <td>{!! $data['phone'] !!}</td>
                            <td>{!! $data['email'] !!}</td>
                        </tr>
                    </tbody>
                </table>
            </div>
        </div>
    </body>
</html>
