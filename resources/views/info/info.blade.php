<table class="table table-striped">
    <tbody>
        <tr data-id="{!! $data['id'] !!}" class="info-clickable">
            <td>icon</td><td id="info-icon">{!! $data['icon'] !!}</td>
            <td>date</td><td id="info-date">{!! $data['date'] !!}</td>
            <td>name</td><td id="info-name">{!! $data['name'] !!}</td>
            <td>phone</td><td id="info-phone">{!! $data['phone'] !!}</td>
            <td>email</td><td id="info-email">{!! $data['email'] !!}</td>
        </tr>
            @forelse($data['additional_fields'] as $label=>$field)
                <tr>
                    <td>{!! $field['label'] !!}</td>
                    <td>{!! $field['field'] !!}</td>
                </tr>
            @empty
            @endforelse
    </tbody>
</table>